import React, { useState, useCallback } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  makeStyles,
  Typography
} from "@material-ui/core";
import ArrowForwardIosRoundedIcon from "@material-ui/icons/ArrowForwardIosRounded";
import ArrowBackIosRoundedIcon from "@material-ui/icons/ArrowBackIosRounded";
import DayHeader from "./components/dayheader";
import Week, { Day } from "./components/week";
import getDay from "date-fns/getDay";
import getMonth from "date-fns/getMonth";
import getDaysInMonth from "date-fns/getDaysInMonth";
import format from "date-fns/format";
import DayInputForm from "./components/dayinputform";
import { getYear } from "date-fns";

const useStyles = makeStyles(theme => ({
  root: {
    margin: "12px"
  },
  header: {
    display: "flex",
    justifyContent: "space-between"
  },
  backButton: {
    left: theme.spacing(2),
    position: "fixed"
  },
  nextButton: {
    right: theme.spacing(2),
    position: "fixed"
  },
  buttonAppBar: {
    top: "auto",
    bottom: 0,
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  }
}));

const App: React.FC = () => {
  const classes = useStyles();
  const monthTitles: string[] = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];
  const [monthIndex, setMonthIndex] = useState(getMonth(new Date())); //monthIndex starts from 0
  const currentYear = getYear(new Date());
  console.log(`current month: ${monthIndex}`);
  console.log(`current year: ${currentYear}`);

  const firstDay = getDay(new Date(2020, monthIndex, 1)); //monthIndex starts from 0, sunday = 0

  const daysInMonth = getDaysInMonth(new Date(2020, monthIndex)); //monthIndex starts from 0
  console.log(`firstDay: ${firstDay}`);
  console.log(`daysInMonth: ${daysInMonth}`);

  const [dayList, setDayList] = useState<Map<string, string>>(new Map());

  function manipulateDays(value: any, index: number): Day {
    const firstDayIndex = firstDay - 1 < 0 ? 6 : firstDay - 1;
    const day = index + 1 - firstDayIndex;
    const date = new Date(2020, monthIndex, day);
    const key = format(date, "d MMMM yyyy");
    if (day < 1 || day > daysInMonth) {
      return {
        title: "",
        description: "",
        disable: true
      };
    }
    return {
      title: `${day}`,
      description: dayList.get(key) === undefined ? "" : dayList.get(key)!,
      disable: false,
      date: date
    };
  }

  const days = [...Array(42)].map<Day>(manipulateDays);

  const clickNextHandler = useCallback(() => {
    console.log("button next is clicked");
    const nextIndex = monthIndex + 1;
    setMonthIndex(nextIndex);
  }, [monthIndex]);

  const clickBackHandler = useCallback(() => {
    console.log("button back is clicked");
    const previousIndex = monthIndex - 1;
    setMonthIndex(previousIndex);
  }, [monthIndex]);

  const [openDayInputForm, setOpenDayInputForm] = useState(false);
  const [selectedDayTitle, setSelectedDayTitle] = useState("");
  const [selectedDayDescription, setSelectedDayDescription] = useState("");

  return (
    <div className={classes.root}>
      <AppBar elevation={0} position="static" color="secondary">
        <Toolbar className={classes.header}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="previous month button"
            onClick={clickBackHandler}
            disabled={monthIndex === 0}
          >
            <ArrowBackIosRoundedIcon />
          </IconButton>
          <Typography variant="h5">{`${monthTitles[
            monthIndex
          ].toLocaleUpperCase()}   2020`}</Typography>
          <IconButton
            edge="end"
            color="inherit"
            aria-label="next month button"
            onClick={clickNextHandler}
            disabled={monthIndex === 11}
          >
            <ArrowForwardIosRoundedIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
      <DayHeader />
      <Week
        days={days}
        onDayClick={index => {
          setOpenDayInputForm(true);
          const selectedDate = days[index].date!;
          const title = format(selectedDate, "d MMMM yyyy");
          const description =
            dayList.get(title) === undefined ? "" : dayList.get(title)!;
          setSelectedDayTitle(title);
          setSelectedDayDescription(description);
          console.log(`click on day : ${title}`);
          console.log(`day description : ${description}`);
        }}
      />
      <DayInputForm
        date={selectedDayTitle}
        isOpen={openDayInputForm}
        onClose={() => setOpenDayInputForm(false)}
        onSave={discription => {
          dayList.set(selectedDayTitle, discription);
          console.log(dayList);
          setOpenDayInputForm(false);
        }}
        description={selectedDayDescription}
      />
      <AppBar
        elevation={0}
        position="static"
        color="secondary"
        className={classes.buttonAppBar}
      >
        <Toolbar className={classes.header}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="previous month button"
            onClick={clickBackHandler}
            disabled={monthIndex === 0}
          >
            <ArrowBackIosRoundedIcon />
          </IconButton>
          <Typography variant="h5">{`${monthTitles[
            monthIndex
          ].toLocaleUpperCase()}   2020`}</Typography>
          <IconButton
            edge="end"
            color="inherit"
            aria-label="next month button"
            onClick={clickNextHandler}
            disabled={monthIndex === 11}
          >
            <ArrowForwardIosRoundedIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default App;
