import React, { FC } from "react";
import {
  makeStyles,
  Tooltip,
  useTheme,
  useMediaQuery,
  ButtonBase,
  Typography
} from "@material-ui/core";
import clsx from "clsx";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    backgroundColor: theme.palette.secondary.main,
    color: theme.palette.secondary.contrastText,
    [theme.breakpoints.down("xs")]: {
      backgroundColor: theme.palette.secondary.contrastText,
      color: theme.palette.secondary.main
    }
  },
  disabledRoot: {
    [theme.breakpoints.down("xs")]: {
      display: "none"
    }
  },
  button: {
    border: 1,
    width: "100%",
    height: "100%",
    fontSize: "16px"
  },
  buttonWithMark: {
    width: "100%",
    height: "100%",
    fontSize: "16px",
    [theme.breakpoints.up("sm")]: {
      "&::before": {
        content: `''`,
        position: "absolute",
        borderStyle: "solid",
        borderWidth: "10px",
        borderLeftColor: theme.palette.secondary.contrastText,
        borderTopColor: theme.palette.secondary.contrastText,
        borderRightColor: "transparent",
        borderBottomColor: "transparent",
        top: "0",
        left: "0"
      }
    }
  },
  titleContent: {
    display: "flex",
    [theme.breakpoints.down("xs")]: {
      width: "10%",
      padding: "20px"
    }
  },
  descriptionContent: {
    display: "flex",
    width: "90%",
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  }
}));

interface DayButtonProbs {
  title: string;
  description: string;
  disabled: boolean;
  index: number;
  onClick: (index: number) => void;
}

const DayButton: FC<DayButtonProbs> = props => {
  const classes = useStyles(props);
  const theme = useTheme();
  const upSm = useMediaQuery(theme.breakpoints.up("sm"));
  const { title, description, disabled, onClick, index } = props;
  return (
    <div
      className={clsx(classes.root, {
        [classes.disabledRoot]: disabled
      })}
    >
      <Tooltip
        title={description}
        arrow
        disableHoverListener={description.length === 0 || !upSm}
      >
        <ButtonBase
          className={clsx(classes.button, {
            [classes.buttonWithMark]: description.length > 0
          })}
          disabled={disabled}
          onClick={() => onClick(index)}
        >
          <div className={classes.titleContent}>
            <Typography variant="body1" align="center">
              {title}
            </Typography>
          </div>
          <div className={classes.descriptionContent}>
            <Typography variant="body1" align="left">
              {description}
            </Typography>
          </div>
        </ButtonBase>
      </Tooltip>
    </div>
  );
};

export default DayButton;
