import React, { FC } from "react";
import { makeStyles, Typography } from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  dayHeaders: {
    color: theme.palette.info.contrastText,
    backgroundColor: theme.palette.secondary.main,
    display: "flex",
    flexFlow: "row wrap",
    alignItems: "center",
    justifyContent: "space-around",
    height: "50px",
    "& > *": {
      width: `${100 / 7}%`
    },
    [theme.breakpoints.down("xs")]: {
      display: "none"
    }
  }
}));

const DayHeader: FC = props => {
  const classes = useStyles(props);
  return (
    <div key="weekdays" className={classes.dayHeaders}>
      <Typography variant="h6" align="center">
        MON
      </Typography>
      <Typography variant="h6" align="center">
        TUE
      </Typography>
      <Typography variant="h6" align="center">
        WEN
      </Typography>
      <Typography variant="h6" align="center">
        THU
      </Typography>
      <Typography variant="h6" align="center">
        FRI
      </Typography>
      <Typography variant="h6" align="center">
        SAT
      </Typography>
      <Typography variant="h6" align="center">
        SUN
      </Typography>
    </div>
  );
};

export default DayHeader;
