import React, { FC, useState, ChangeEventHandler } from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  TextField,
  DialogActions,
  Button
} from "@material-ui/core";

interface DayInputFormProbs {
  date: string;
  description: string;
  isOpen: boolean;
  onClose: () => void;
  onSave: (description: string) => void;
}

const DayInputForm: FC<DayInputFormProbs> = props => {
  const { date, isOpen, onClose, onSave } = props;
  const [description, setDescription] = useState("");

  const updateDescription: ChangeEventHandler<HTMLInputElement> = event => {
    if (event.target.value.length <= 40) {
      setDescription(event.target.value);
    }
  };

  return (
    <div>
      <Dialog
        open={isOpen}
        aria-labelledby="form-dialog-titlee"
        fullWidth
        onEnter={() => setDescription(props.description)}
      >
        <DialogTitle id="form-dialog-title">
          {date.toLocaleUpperCase()}
        </DialogTitle>
        <DialogContent>
          <TextField
            color="secondary"
            autoFocus
            margin="dense"
            id="description"
            label="DESCRIPTION"
            type="description"
            value={description}
            onChange={updateDescription}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="secondary">
            Cancel
          </Button>
          <Button
            variant="contained"
            color="secondary"
            onClick={() => onSave(description)}
          >
            Save
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default DayInputForm;
