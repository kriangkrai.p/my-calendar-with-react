import React, { FC } from "react";
import { makeStyles } from "@material-ui/core";
import DayButton from "../components/daybutton";

const useStyles = makeStyles(theme => ({
  dayHeaders: {
    position: "relative",
    display: "flex",
    flexFlow: "row wrap",
    "& > *": {
      width: `${100 / 7}%`,
      height: theme.spacing(13)
    },
    [theme.breakpoints.down("xs")]: {
      "& > *": {
        width: "100%",
        height: theme.spacing(7)
      },
      flexDirection: "column"
    }
  }
}));

interface WeekProbs {
  days: Day[];
  onDayClick: (index: number) => void;
}

export interface Day {
  title: string;
  description: string;
  disable: boolean;
  date?: Date;
}

const Week: FC<WeekProbs> = props => {
  const classes = useStyles(props);
  const { days, onDayClick } = props;
  return (
    <div className={classes.dayHeaders}>
      {days.map((day, index) => (
        <DayButton
          key={index}
          title={day.title}
          description={day.description}
          disabled={day.disable}
          onClick={onDayClick}
          index={index}
        ></DayButton>
      ))}
    </div>
  );
};

export default Week;
